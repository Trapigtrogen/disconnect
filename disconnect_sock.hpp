#ifndef DISCONNECT_SOCK_H
#define DISCONNECT_SOCK_H

#if defined(_MSC_VER) || defined(__GNUG__)
#define d_packed_struct(name,struct_body) _Pragma("pack(push,1)") struct name { struct_body }; _Pragma("pack(pop)")
#else
#define d_packed_struct(name,struct_body)  struct __attribute__((packed)) name { struct_body };
#endif

#include <cstdint>

#if defined(DISCONNECT_OS_WIN)
#include <winsock2.h>
#include <Ws2tcpip.h>

using d_socket = SOCKET;
using socklen_t = int32_t;

inline int winsock_setup(){
    WSADATA wd; 
    if(WSAStartup(MAKEWORD(2,2),&wd) != NO_ERROR)
        return 0;
    return WSAGetLastError();
}

#define dSetup() winsock_setup()
#define dCleanup() WSACleanup()
#define dLastError() WSAGetLastError()
#define dIoctlsocket(sock, cmd, argp) ioctlsocket(sock,cmd,argp)

#define D_EWOULDBLOCK WSAEWOULDBLOCK
#define D_ENETRESET WSAENETRESET
#define D_ETIMEDOUT WSAETIMEDOUT
#define D_ECONNRESET WSAECONNRESET
#define D_ECONNABORTED WSAECONNABORTED

#elif defined(DISCONNECT_OS_UNIX)
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <cstring>
#include <unistd.h>
#include <sys/select.h>
#include <sys/ioctl.h>

using d_socket = int;
using sockaddr = struct sockaddr;

#define dSetup() 0
#define dCleanup() 0
#define dLastError() errno
#define dIoctlsocket(sock, cmd, argp) ioctl(sock,cmd,argp)

#define D_EWOULDBLOCK EWOULDBLOCK
#define D_ENETRESET ENETRESET
#define D_ETIMEDOUT ETIMEDOUT
#define D_ECONNRESET ECONNRESET
#define D_ECONNABORTED ECONNABORTED

#define SOCKET_ERROR (-1)
#define INVALID_SOCKET  (-1)

#define closesocket(s) close(s);

#ifndef FIONBIO
    #define FIONBIO SOCK_NONBLOCK
#endif

#else
#error "Unsupported platform"
#endif

#define ERROR_OK 0

using d_port_t = uint16_t;

inline constexpr uint32_t recv_buffer_size = 8192;
inline constexpr int32_t server_timeout = 20000;

//increase size when needed
//use sizeof(DMessageCode) to get the size of this
//all enums are subject to change atm
enum struct DMessageCode : uint16_t
{
    Handshake1,             //1st part of handshake (and currently only part)
    Handshake2,             //add proper handshake and encrypt later
    LoginRequest,           //request a login (expect DUserInfoHeader in message)
    RegisterRequest,        //request registeration (expect DUserInfoHeader in message)
    BadLogin,               //login failed. rest of message should be the error message (e.g. "Invalid password")
    BadRegister,            //register failed. rest of message should be the error
    GoodLogin,              //login succeeeded, server will receive chat messages now
    GoodRegister,           //register succeeded, server will receive chat messages now
    ChatMessage,            //a chat message. expect appropriate header in message (server->client and client->server are different)
    ChatMessageConfirm,     //confirm that message was received (by server). expect DMessageConfirm
    ChatMessageError,       //chat message was received, but there was an error when handling it
    EndSession,             //end a session (unused for now)
};

//serverside state enum
enum struct DServerConnectionState : uint8_t
{
    Handshake1,
    Handshake2,
    PreLogin,       //handshake done, waiting for login or register
    Idle,           //logged in, waiting for chatmessages
    Terminated,     //connection is terminated
};


//clientside state enum
enum struct DClientConnectionState : uint8_t
{

};

//send when trying to login or register
//name and password should come after this:
/*
msg
{
    (4 bytes) DMessageHeader
    (4 bytes) DUserInfoHeader
    (x bytes) username
    (x bytes) password
}
*/
struct DUserInfoHeader
{
    uint16_t name_len;
    uint16_t pass_len;
};

//chat message from client to server
//t_id = clients temp id for message
//channel_id = channel where the message is being sent
//msg_size = message length
//message itself comes after this header
struct DChatMessageHeader1
{
    uint32_t t_id;
    uint16_t channel_id;
    uint16_t msg_size;
};

//chat message from server to client
//msg_id = message id
//channel_id = channel where the message is being sent
//sender_name_len = length of sender name
//message_len = message length
//sender username and message should come after this header in that order
d_packed_struct
(
    DChatMessageHeader2,
    uint32_t msg_id;
    uint16_t channel_id;
    uint16_t sender_name_len;
    uint16_t message_len;
)

//server to client confirmation that message was received
//t_id = clients tmp id for the message
//msg_id = id given to message by server
//channel_id = channel where the message was sent from
d_packed_struct
(
    DMessageConfirm,
    uint32_t t_id;
    uint32_t msg_id;
    uint16_t channel_id;
)

struct DMessageHeader
{
    DMessageCode code;
    uint16_t size;      //message size (including this header size)
};

inline int CreateSocketNB() {
    // Create Socket is non-blocking mode
#ifdef DISCONNECT_OS_WIN
    d_socket thisSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    // non-blocking mode
    u_long iMode = 1;
    ioctlsocket(thisSocket, FIONBIO, &iMode);
#else
    d_socket thisSocket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP);
#endif
    if (thisSocket < 0) {
        return -1;
    }
    return thisSocket;
}

#endif /* DISCONNECT_SOCK_H */
