#include "loginscreen.h"
#include "ui_loginscreen.h"

LoginScreen::LoginScreen(int _index, QString serverName, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginScreen)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);

    connect(ui->RegisterBtn, SIGNAL(clicked()), this, SLOT(SendRegisterData()));
    connect(ui->LoginBtn, SIGNAL(clicked()), this, SLOT(SendLoginData()));
    connect(ui->CancelBtn, SIGNAL(clicked()), this, SLOT(close()));

    index = _index;

    ui->TitleLabel->setText("Login to: " + serverName);
}

LoginScreen::~LoginScreen()
{
    delete ui;
}

void LoginScreen::SendRegisterData() {
    emit ReturnData(index, ui->UserBox->text(), ui->PassBox->text(), true);
    //close();
}

void LoginScreen::SendLoginData() {
    emit ReturnData(index, ui->UserBox->text(), ui->PassBox->text(), false);
    //close();
}
