#ifndef CHATWIDGET_H
#define CHATWIDGET_H

#include <QFormLayout>

#include <QListWidget>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QTimer>

#include <vector>
#include <mutex>

#include <QDebug>

#include "chatmessagedisplay.h"
#include "webclient.h"

//TODO:
//  - add client connection stuff to this widget
//  - connect inputbox to a handler that sends the message to server and to the chat display
//      - use tmp id, and replace id after server gives the real id in response
//      - if no server response after x amount of time (few seconds), indicate to user that the message
//        was not sent (red message bg or something). Remove the indication if server responds later
class ChatWidget : public QWidget
{
    Q_OBJECT

public Q_SLOTS:
    void check_messages()
    {
        inc_message_mutex.lock();
        if(incoming_messages.size() > 0)
        {
            for(uint32_t i = 0; i < incoming_messages.size(); ++i)
            {
                display->AddMessage(incoming_messages[i].sender_name,
                                    incoming_messages[i].message,
                                    QString::number(incoming_messages[i].msg_id));
            }
            incoming_messages.clear();
        }
        inc_message_mutex.unlock();
    }

public:
    ChatWidget(QString _ip, QString username, QString password, bool newUser)
    {
        auto lay = new QFormLayout();
        setLayout(lay);

        display = new ChatMessageDisplay();
        display->Init();

        auto chat_and_input = new QWidget();

        auto clay = new QVBoxLayout();

        clay->setContentsMargins(10,0,10,0);

        chat_and_input->setLayout(clay);

        clay->addWidget(display->GetWidget());

        auto inputs = new QWidget();
        auto ilay = new QHBoxLayout();

        inputs->setMaximumHeight(150);

        ilay->setContentsMargins(0,10,0,0);

        inputs->setLayout(ilay);

        texbox = new QPlainTextEdit();
        ilay->addWidget(texbox);

        auto btn = new QPushButton();
        btn->setMinimumHeight(150);
        btn->setMinimumWidth(150);
        ilay->addWidget(btn);

        clay->addWidget(inputs);

        channel_list = new QListWidget();

        lay->setWidget(0,QFormLayout::ItemRole::LabelRole,channel_list);
        lay->setWidget(0,QFormLayout::ItemRole::FieldRole,chat_and_input);

        // Connect to server
        connection = new WebClient(_ip,this);
        connectionResult = connection->Connect(username, password, newUser);
        connectedUser = username;

        // DEBUG: TMP
        display->AddMessage("TERO","moikka moi jätkät","123", "#00fab5");

        connect(btn,&QPushButton::pressed,this,&ChatWidget::OnMessageSend);

        QTimer* tmr = new QTimer(this);
        connect(tmr,SIGNAL(timeout()),this,SLOT(check_messages()));
        tmr->start(100);
    }

    int connectionResult;
    QString connectedUser;

    virtual ~ChatWidget() = default;

    void OnMessageSend()
    {
        QString text = texbox->toPlainText();
        if(text.length() > 0)
        {
            auto tmp_id = tmp_id_gen();
            display->AddMessage(connectedUser,text,"tmp_" + QString::number(tmp_id));
            texbox->clear();

            //send message here vv
            connection->SendChatMessage(tmp_id,0,text);
        }
    }

    void OnMessageReceive(uint32_t msg_id, uint16_t channel_id, const char* sender_name, const char* message)
    {
        //channel id unused for now

        qDebug() << sender_name << "Says: " << message;

        //display->AddMessage(QString(sender_name),QString(message),QString::number(msg_id));
        inc_message_mutex.lock();
        incoming_messages.push_back(MsgData{msg_id,channel_id,QString(sender_name),QString(message)});
        inc_message_mutex.unlock();
    }

    void OnMessageConfirm(uint32_t tmp_id, uint32_t msg_id, uint16_t channel_id)
    {
        //change message id from tmp_id to msg_id
    }

    void OnClose()
    {
        delete connection;
        qDebug() << "Closed tab";
    }

    WebClient* connection;

private:

    static uint32_t tmp_id_gen()
    {
        static uint32_t num = 0u;
        return ++num;
    }

    ChatMessageDisplay* display;
    QPlainTextEdit* texbox;
    QListWidget* channel_list;

    struct MsgData
    {
        uint32_t msg_id;
        uint16_t msg_channel;
        QString sender_name;
        QString message;
    };

    std::vector<MsgData> incoming_messages;
    std::mutex inc_message_mutex;
};

#endif // CHATWIDGET_H
