#ifndef SERVERINFO_H
#define SERVERINFO_H

#include <cstdint>
#include <QString>

using address_t = uint32_t;

struct ServerInfo
{
    address_t address = 0;
    uint16_t port = 0;
    QString addressString = "";
    QString preferredName = "";
    QString nickName = "";
    bool autologin = false;
};

#endif // SERVERINFO_H
