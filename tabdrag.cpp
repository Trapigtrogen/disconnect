#include "tabdrag.h"
#include "chattabwidget.h"
#include "chatwindow.h"

TabDrag::TabDrag(QObject *dragSource) : QDrag(dragSource){
    //connect(this,&TabDrag::actionChanged,this,&TabDrag::OnActionChanged);
    //connect(this,&TabDrag::targetChanged,this,&TabDrag::OnTargetChanged);
}

TabDrag::~TabDrag()
{
    auto data = (DragTabData*)source();

    if(target() == nullptr && (data->tbar->count() > 1 || data->fromMain))
    {
        auto wind = data->tbar->window();

        if(!wind->rect().contains(wind->mapFromGlobal(QCursor::pos())))
        {
            qDebug() << "make new";

            ChatWindow* cw = new ChatWindow();

            cw->tabs->addTab(data->tab,data->tabname);

            cw->show();

            ((DragTabData*)source())->tryCloseWindow();

        }
    }

    source()->deleteLater();
}

void DragTabData::tryCloseWindow()
{
    if(tbar->count() == 0)
    {
        auto wind = dynamic_cast<ChatWindow*>(tbar->window());

        if(wind)
        {
            wind->close();
        }
    }
}

