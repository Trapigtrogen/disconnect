#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include <QWidget>
#include <QHBoxLayout>
#include "chattabwidget.h"

class ChatWindow : public QWidget
{
    Q_OBJECT
public:

    ChatWindow()
    {
        //setMinimumHeight(300);
        //setMinimumWidth(300);
        setAcceptDrops(true);
        setAttribute(Qt::WA_DeleteOnClose);
        QHBoxLayout* layout = new QHBoxLayout(this);


        tabs = new ChatTabWidget();

        layout->addWidget(tabs);
    }
    virtual ~ChatWindow() {}

    virtual void closeEvent(QCloseEvent* e)
    {
        tabs->OnWindowClose();
    }

    virtual void dragEnterEvent(QDragEnterEvent* e) override{
        e->acceptProposedAction();
    }

    virtual void dragLeaveEvent(QDragLeaveEvent* e) override{
        e->accept();
    }

    virtual void dropEvent(QDropEvent* e) override
    {
        DragTabData* data = dynamic_cast<DragTabData*>(e->source());
        if(data && data->tbar == tabs->tabBar())
            return;

        e->setDropAction(Qt::MoveAction);
        e->accept();
        tabs->addTab(data->tab,data->tabname);
        data->tryCloseWindow();
    }


    ChatTabWidget* tabs = nullptr;
};

#endif // CHATWINDOW_H
