QT       += core gui webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 static

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

win32:DEFINES += DISCONNECT_OS_WIN
unix:DEFINES += DISCONNECT_OS_UNIX


# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    chattabwidget.cpp \
    loginscreen.cpp \
    main.cpp \
    mainwindow.cpp \
    serverscreen.cpp \
    tabdrag.cpp \
    webclient.cpp

HEADERS += \
    chatmessagedisplay.h \
    chattabwidget.h \
    chatwidget.h \
    chatwindow.h \
    loginscreen.h \
    mainwindow.h \
    serverinfo.h \
    serverscreen.h \
    tabdrag.h \
    webclient.h \
    disconnect_sock.hpp

FORMS += \
    loginscreen.ui \
    mainwindow.ui \
    serverscreen.ui

win32:DEFINES += DISCONNECT_OS_WIN
unix:DEFINES += DISCONNECT_OS_UNIX
win32:LIBS +=  libws2_32

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
