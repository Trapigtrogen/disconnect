#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "loginscreen.h"
#include "serverscreen.h"
#include "chattabwidget.h"

#include "webclient.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    LoginScreen *lScreen;
    ServerScreen *sScreen;
    ChatTabWidget* tabs = nullptr;
    std::vector<WebClient*> connectedServers;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    virtual void dragEnterEvent(QDragEnterEvent* e) override;
    virtual void dragLeaveEvent(QDragLeaveEvent* e) override;
    virtual void dropEvent(QDropEvent* e) override;

private:
    Ui::MainWindow *ui;
    void OpenLoginWindow(int index);
    void OpenServerWindow(int index);
    void UpdateFile(std::string filename, std::string data);
    std::vector<std::string> ReadFile(std::string filename);
    std::string GetFile(std::string filename);
    void SaveServerList();
    void CheckDarkTheme();

    void closeEvent(QCloseEvent*);

private slots:
    // Login Screen
    void GetLoginData(int index, QString username, QString password, bool newUser);
    void OnLoginBtnPress();

    // Server Screen
    void GetServerData(int index, QString ip, QString name);
    void AddServer();
    void EditServer();
    void DeleteServer();

    void showContextMenu(const QPoint &pos);

};
#endif // MAINWINDOW_H
