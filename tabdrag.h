#ifndef TABDRAG_H
#define TABDRAG_H

#include <QDrag>
#include <QString>
#include <QObject>
#include <QDebug>

class QTabBar;

struct DragTabData : public QObject
{
    QTabBar* tbar;
    QWidget* tab;
    QString tabname;
    bool fromMain;

    void tryCloseWindow();
};

class TabDrag : public QDrag
{
    Q_OBJECT
public:

    explicit TabDrag(QObject *dragSource);

    virtual ~TabDrag();

    /*
    void OnTargetChanged(QObject* nt)
    {
        qDebug() << nt;
    }

    void OnActionChanged(Qt::DropAction a)
    {
        qDebug() << a;
    }
    */
};


#endif // TABDRAG_H
