#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    dSetup();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
