#ifndef SERVERSCREEN_H
#define SERVERSCREEN_H

#include <QDialog>

namespace Ui {
class ServerScreen;
}

class ServerScreen : public QDialog
{
    Q_OBJECT

public:
    explicit ServerScreen(int index, QString _ip, QString _name, QWidget *parent = nullptr);
    explicit ServerScreen(QWidget *parent = nullptr);
    ~ServerScreen();

private:
    Ui::ServerScreen *ui;
    int serverIndex;

private slots:
    void SaveData();

signals:
    void ReturnData(int index, QString ip, QString name);
};

#endif // SERVERSCREEN_H
