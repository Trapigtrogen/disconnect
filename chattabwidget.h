#ifndef CHATTABWIDGET_H
#define CHATTABWIDGET_H

#include <QTabWidget>
#include <QTabBar>
#include <QEvent>
#include <QMouseEvent>
#include <QRegion>
#include <QDrag>
#include <QPainter>
#include <QMimeData>

#include <QVBoxLayout>
#include <QHBoxLayout>

#include "tabdrag.h"
#include "chatmessagedisplay.h"
#include "chatwidget.h"

class ChatTabWidget : public QTabWidget
{
public:
    ChatTabWidget()
    {
        setAcceptDrops(true);
        setMovable(true);
        tabBar()->setMouseTracking(true);
        setTabsClosable(true);

        connect(this,&ChatTabWidget::tabCloseRequested,this,&ChatTabWidget::OnCloseTab);
    }

    void OnCloseTab(int i)
    {
        auto w = widget(i);
        ((ChatWidget*)w)->OnClose();
        removeTab(i);
        delete w;
    }

    void OnWindowClose()
    {
        for(unsigned i = 0; i < count(); ++i)
        {
            auto w = widget(i);
            ((ChatWidget*)w)->OnClose();
            delete w;
        }
    }

    virtual void mouseMoveEvent(QMouseEvent* e) override;


    virtual void dragEnterEvent(QDragEnterEvent* e) override{
        e->accept();
    }

    virtual void dragLeaveEvent(QDragLeaveEvent* e) override{
        e->accept();
    }

    virtual void dropEvent(QDropEvent* e) override
    {
        DragTabData* data = dynamic_cast<DragTabData*>(e->source());
        if(data && data->tbar == tabBar())
            return;

        e->setDropAction(Qt::MoveAction);
        e->accept();
        addTab(data->tab,data->tabname);
        data->tryCloseWindow();
    }
};

#endif // CHATTABWIDGET_H
