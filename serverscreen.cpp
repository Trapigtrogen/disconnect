#include "serverscreen.h"
#include "ui_serverscreen.h"

ServerScreen::ServerScreen(int _index, QString _ip, QString _name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerScreen)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);

    connect( ui->SaveBtn, SIGNAL(clicked()), this, SLOT(SaveData()) );
    connect( ui->CancelBtn, SIGNAL(clicked()), this, SLOT(close()) );

    serverIndex  = _index;
    ui->TitleLabel->setText("Edit server " + _name);
    ui->IPBox->setText(_ip);
    ui->NameBox->setText(_name);
}

ServerScreen::ServerScreen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerScreen)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);

    connect( ui->SaveBtn, SIGNAL(clicked()), this, SLOT(SaveData()) );
    connect( ui->CancelBtn, SIGNAL(clicked()), this, SLOT(close()) );

    serverIndex  = -1;
    ui->TitleLabel->setText("Add new server");
}

ServerScreen::~ServerScreen()
{
    delete ui;
}

void ServerScreen::SaveData() {
    emit ReturnData(serverIndex, ui->IPBox->text(), ui->NameBox->text());
    close();
}
