#ifndef WEBCLIENT_H
#define WEBCLIENT_H

#include <QtDebug>

#include <iostream>
#include <chrono>
#include <future>
#include <string>
#include <thread>
#include <mutex>

#include <QMessageBox>

#include "disconnect_sock.hpp"

class ChatWidget;

class WebClient
{
private:
    sockaddr_in ParseAddr(std::string addr);
    sockaddr_in IP;

    d_socket thisSocket; // socket id

    char sendBuffer[32] = "";
    char recvBuffer[32] = "";

    ChatWidget* widget;
    std::thread receive_thread;
    std::mutex socket_mut;
    std::promise<void> receive_exit_signal;
    std::shared_future<void> recv_exit_fut;

    enum connectionErrorTypes {
        null,
        connection,
        handshake,
        authentication
    };

public:
    WebClient(QString _rawIp, ChatWidget* _widget);
    ~WebClient();

    int Connect(QString username, QString password, bool newUser);
    int Disconnect();
    int Authentication(QString _username, QString _password, bool newUser);

    int SendChatMessage(uint32_t tmp_id, uint32_t channel, const QString& message );

    static void RecvMessages(WebClient* client, std::shared_future<void> exit_fut);

    connectionErrorTypes connectionError = null;
};

#endif // WEBCLIENT_H
