#include "webclient.h"
#include "chatwidget.h"

WebClient::WebClient(QString _rawIp, ChatWidget* _widget) {
    widget = _widget;
    IP = ParseAddr(_rawIp.toStdString());
    qDebug() << "Connection to server: " << _rawIp;

    // Nonblocking socket
    //thisSocket = CreateSocketNB();

    // Blocking mode is better when connecting
    thisSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

WebClient::~WebClient()
{
    receive_exit_signal.set_value();

    if(receive_thread.joinable())
        receive_thread.join();

    Disconnect();
}

int WebClient::Connect(QString username, QString password, bool newUser) {
// CONNECT TO HOST
    auto err = connect(thisSocket, (sockaddr*)&IP, sizeof(IP));
    if (err == SOCKET_ERROR) {
        qDebug() << "Socket Connection FAILED!";
        
        auto err2 = dLastError();
        qDebug() << "Error code: " << err;
        
        if (thisSocket > 0) { closesocket(thisSocket); }
        connectionError = connection;
        return -1;
    }
    qDebug() << "Connected to socket";


// HANDSHAKE
    DMessageHeader header = { DMessageCode::Handshake1, sizeof(DMessageHeader) };
    if(send(thisSocket, (char*)&header, sizeof(DMessageHeader), 0) < 0) {
        qDebug() << "Error while sending handshake 1. Errno: " << dLastError();
        connectionError = handshake;
        return -1;
    }

    // Get the respond
    uint32_t respond_size = sizeof(DMessageHeader);
    char* respond_buffer = new char[respond_size];

    int result = recv(thisSocket, respond_buffer, respond_size, 0);
    if(result < 1) {
        qDebug() << "Error receiving handshake 1";
        return -1;
    }
    DMessageHeader* respondHeader = (DMessageHeader*)respond_buffer;
    if(respondHeader->code != DMessageCode::Handshake1) {
        qDebug() << "Handshake failed";
        connectionError = handshake;
        return -1;
    }


// LOGIN / REGISTER
    if( Authentication(username, password, newUser) != 0 ) {
        if (thisSocket > 0) { closesocket(thisSocket); }
        return -1;
    }

    delete[] respond_buffer;

    static unsigned long fl = 1;
    dIoctlsocket(thisSocket,FIONBIO,&fl);

    recv_exit_fut = receive_exit_signal.get_future();
    receive_thread = std::thread(WebClient::RecvMessages,this,recv_exit_fut);
    return 0;
}

int WebClient::Authentication(QString _username, QString _password, bool newUser) {
// SEND LOGIN / REGISTER DATA
    std::string username = _username.toStdString();
    std::string password = _password.toStdString();
    uint16_t username_length = username.length() + 1;
    uint16_t password_length = password.length() + 1;

    uint16_t msg_size = sizeof(DMessageHeader)+sizeof(DUserInfoHeader)+username_length+password_length;
    char* msg_buffer = new char[msg_size];

    if(!newUser) { // Login
        *(DMessageHeader*)msg_buffer = DMessageHeader{DMessageCode::LoginRequest, msg_size};
    }
    else { // Register
        *(DMessageHeader*)msg_buffer = DMessageHeader{DMessageCode::RegisterRequest, msg_size};
    }

    *(DUserInfoHeader*)(msg_buffer+sizeof(DMessageHeader)) = DUserInfoHeader{username_length, password_length};

    memcpy(msg_buffer+sizeof(DMessageHeader)+sizeof(DUserInfoHeader), username.data(), username.length()+1);
    memcpy(msg_buffer+sizeof(DMessageHeader)+sizeof(DUserInfoHeader)+username.length()+1, password.data(), password.length()+1);

    int result = send(thisSocket, msg_buffer, msg_size, 0);
    if(result < 1) {
        connectionError = authentication;
        return -1;
    }


// RESPOND
    uint16_t respond_size = 8192;
    char* respond_buffer = new char[respond_size];

    // Get respond
    result = recv(thisSocket, respond_buffer, respond_size, 0);
    if(result < 1) {
        qDebug() << "Error receiving answer to login / register";
        connectionError = authentication;
        return -1;
    }

    // Parse
    char* respondHeader_buffer = new char[sizeof(DMessageHeader)];
    memcpy(respondHeader_buffer, respond_buffer, sizeof(DMessageHeader));
    DMessageHeader* respondHeader = (DMessageHeader*)respondHeader_buffer;

    uint16_t rmsg_size = respondHeader->size;
    char* rmsg_buffer = new char[rmsg_size];
    memcpy(rmsg_buffer, respond_buffer+sizeof(DMessageHeader), rmsg_size);

    // Check respond data
    if(respondHeader->code != DMessageCode::GoodLogin && respondHeader->code != DMessageCode::GoodRegister) {
        connectionError = authentication;
        QString str = rmsg_buffer;

        QMessageBox msgBox;
        msgBox.setText(str);
        msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
        msgBox.exec();

        return -1;
    }

    delete[] msg_buffer;
    delete[] respond_buffer;
    delete[] rmsg_buffer;
    delete[] respondHeader_buffer;
    return 0;
}

int WebClient::Disconnect() {
    closesocket(thisSocket);
    thisSocket = INVALID_SOCKET;
    qDebug() << "Disconnected";
    return 0;
}

sockaddr_in WebClient::ParseAddr(std::string addr) {
    sockaddr_in result;
    memset( &result, 0, sizeof(result) );

    std::remove_if(addr.begin(),addr.end(),isspace);

    bool valid = true;
    bool colon = false;
    for(unsigned i = 0; i < addr.length(); ++i)
    {
        if(!isdigit(addr[i]))
        {
            if(!colon && addr[i] == ':')
                colon = true;
            else if(addr[i] != '.')
            {
                valid = false;
                break;
            }
        }
    }

    if(!valid || !colon)
        return result;

    auto delim = addr.find(':');
    std::string ip = addr.substr(0,delim);
    u_short port = std::atoi((addr.substr(delim+1,addr.length()-delim)).data());

    auto addr_c = inet_addr(ip.data());

    if(addr_c == INADDR_NONE)
        return result;

    result.sin_family = AF_INET;
    result.sin_addr.s_addr = addr_c;
    result.sin_port = htons(port);

    return result;
}

int WebClient::SendChatMessage(uint32_t tmp_id, uint32_t channel, const QString& message )
{
    static char buffer[8192];

    std::string msg = message.toStdString();
    uint16_t message_len = msg.length()+1;
    uint16_t msg_size = sizeof(DMessageHeader) + sizeof(DChatMessageHeader1) + message_len;

    *((DMessageHeader*)buffer) = DMessageHeader{DMessageCode::ChatMessage,msg_size};
    *((DChatMessageHeader1*)(buffer+sizeof(DMessageHeader))) = DChatMessageHeader1{tmp_id,channel,message_len};
    memcpy(buffer + sizeof(DMessageHeader) + sizeof(DChatMessageHeader1),msg.data(),message_len);

    auto t1 = std::chrono::system_clock::now();

    while(1)
    {
        socket_mut.lock();
        int err = send(thisSocket,buffer,msg_size,NULL);
        socket_mut.unlock();

        if((err == SOCKET_ERROR && dLastError() != D_EWOULDBLOCK) ||
                std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()-t1).count() >= 5000)
        {
            //fatal error? or timeout
            //disconnect probably
            Disconnect();
            return 1;
        }
        else
            break;
    }

    return 0;
}


void WebClient::RecvMessages(WebClient* client, std::shared_future<void> exit_fut)
{
    char buffer[8192];
    uint32_t bytes_recvd = 0;

    while(exit_fut.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout)
    {
        //receive from server, after login is done

        client->socket_mut.lock();
        int bytes = recv(client->thisSocket,buffer,8192u,NULL);
        client->socket_mut.unlock();

        if(bytes == SOCKET_ERROR)
        {
            int err = dLastError();
            if(err != D_EWOULDBLOCK)
            {
                client->Disconnect();
                break;
            }
            else
            {
                bytes = 0;
            }
        }
        else if(bytes == 0)
        {
            //connection end
            client->Disconnect();
            break;
        }

        bytes_recvd += bytes;

        if(bytes_recvd >= sizeof(DMessageHeader))
        {
            DMessageHeader header = *(DMessageHeader*)buffer;

            if(bytes_recvd >= header.size)
            {
                bytes_recvd -= header.size;
                qDebug() << bytes_recvd;

                //handle messages
                switch (header.code)
                {
                    case DMessageCode::ChatMessage:
                    {
                        DChatMessageHeader2 msg_header = *(DChatMessageHeader2*)(buffer + sizeof(DMessageHeader));

                        client->widget->OnMessageReceive(msg_header.msg_id,msg_header.channel_id,
                                                         buffer+sizeof(DMessageHeader)+sizeof(DChatMessageHeader2),
                                                         buffer+sizeof(DMessageHeader)+sizeof(DChatMessageHeader2)+msg_header.sender_name_len);
                        break;
                    }
                    case DMessageCode::ChatMessageConfirm:
                    {
                        DMessageConfirm msg_header = *(DMessageConfirm*)(buffer + sizeof(DMessageHeader));
                        client->widget->OnMessageConfirm(msg_header.t_id,msg_header.msg_id,msg_header.channel_id);
                        break;
                    }
                }
            }
        }
    }
}
