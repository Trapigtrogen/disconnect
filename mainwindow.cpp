#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtDebug>
#include "chattabwidget.h"
#include <fstream>
#include <QDir>
#include <QApplication>

// For dark theme
#include <QStyleFactory>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    CheckDarkTheme(); // To follow Windows' dark theme settigns (Linux gets colour palette natively)

    ui->setupUi(this);

    tabs = new ChatTabWidget;

    ((QFormLayout*)ui->centralwidget->layout())->setWidget(0,QFormLayout::ItemRole::FieldRole,tabs);

    //ui->centralwidget->layout()->addWidget(new ChatTabWidget);

    connect( ui->serverListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(OnLoginBtnPress()) );
    connect( ui->addServerButton, SIGNAL(clicked()), this, SLOT(AddServer()) );

    ui->serverListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->serverListWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));

    // Load server list from file. 1 entry/line = 1 server
    std::vector<std::string> readData = ReadFile("servers");
    for (std::string serverInfo : readData) {
        // parser
        size_t pos = 0;
        std::string serverName;
        std::string delimiter = "|";
        pos = serverInfo.find(delimiter);
        serverName = serverInfo.substr(0, pos);
        serverInfo.erase(0, pos + delimiter.length());

        ui->serverListWidget->addItem(serverName.c_str());
        ui->serverListWidget->setCurrentRow(ui->serverListWidget->count() - 1);
        ui->serverListWidget->currentItem()->setData( Qt::UserRole, serverInfo.c_str() );
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    dCleanup();
}

void MainWindow::OpenLoginWindow(int index) {
    ui->serverListWidget->setCurrentRow(index);
    QString serverName = ui->serverListWidget->currentItem()->text();

    lScreen = new LoginScreen(index, serverName); // Is closed inside itself
    connect(lScreen, &LoginScreen::ReturnData, this, &MainWindow::GetLoginData);
    lScreen->show();
}

void MainWindow::OpenServerWindow(int index) {
    if(index < 0) // New server
        sScreen = new ServerScreen();
    else // Existing server
        sScreen = new ServerScreen(index, ui->serverListWidget->currentItem()->data(Qt::UserRole).toString() , ui->serverListWidget->currentItem()->text());
    // Windows close themselves

    connect(sScreen, &ServerScreen::ReturnData, this, &MainWindow::GetServerData);
    sScreen->show();
}

// Deals with data coming from login/register popup
void MainWindow::GetLoginData(int index, QString username, QString password, bool newUser) {
    QString ip = ui->serverListWidget->currentItem()->data(Qt::UserRole).toString();

    ChatWidget* chatWidget = new ChatWidget(ip, username, password, newUser);
    int tabid = tabs->addTab( chatWidget, ui->serverListWidget->currentItem()->text() );
    if(chatWidget->connectionResult != 0) {
        qDebug() << "Creating tab failed: " << chatWidget->connection->connectionError;
        qDebug() << "Error types: 1 Connecting, 2 Handshake, 3 Authentication";
        tabs->removeTab(tabid);
        delete tabs->widget(tabid);
        delete chatWidget;
    }
    else {lScreen->close();}
}

// Deals with server data coming from add/edit server popup
void MainWindow::GetServerData(int index, QString ip, QString name) {
    if(index < 0) { // New server
        ui->serverListWidget->addItem(name);
        ui->serverListWidget->setCurrentRow(ui->serverListWidget->count() - 1);
    }
    else { // Existing server
        ui->serverListWidget->setCurrentRow(index);
        ui->serverListWidget->currentItem()->setText(name);
    }
    ui->serverListWidget->currentItem()->setData( Qt::UserRole, ip );

    SaveServerList();
}

// Saves server list to file
void MainWindow::SaveServerList() {
    std::string data = "";
    for (int i = 0; i < ui->serverListWidget->count(); ++i) {
        ui->serverListWidget->setCurrentRow(i);
        QString qtData = ui->serverListWidget->currentItem()->text();
        data += qtData.toStdString(); // name
        data += "|";
        qtData = ui->serverListWidget->currentItem()->data(Qt::UserRole).toString();
        data += qtData.toStdString(); // ip
        data += "\n";
    }
    UpdateFile("servers", data);
}

void MainWindow::OnLoginBtnPress() { // QT SLOT
    OpenLoginWindow(ui->serverListWidget->currentRow());
}

void MainWindow::AddServer() { // QT SLOT
    OpenServerWindow(-1);
}

void MainWindow::EditServer() { // QT SLOT
    OpenServerWindow(ui->serverListWidget->currentRow());
}

void MainWindow::DeleteServer() {
    // Get curent item on selected row and remove it
    QListWidgetItem *item = ui->serverListWidget->takeItem(ui->serverListWidget->currentRow());
    delete item;

    // Update server list file
    SaveServerList();
}

void MainWindow::showContextMenu(const QPoint &pos) {
    // Handle global position
    QPoint globalPos = ui->serverListWidget->mapToGlobal(pos);

    // Create menu and insert some actions
    QMenu myMenu;
    myMenu.addAction("Edit Server", this, SLOT(EditServer()));
    myMenu.addAction("Remove Server",  this, SLOT(DeleteServer()));

    // Show context menu at handling position
    myMenu.exec(globalPos);
}

std::string MainWindow::GetFile(std::string filename) {
    // PATH TO USER FILES
#ifdef __linux
    std::string path = getenv("HOME");
    path += "/.config/disconnect-client/";
#else
    std::string path = getenv("APPDATA");
    path += "/disconnet-client/";
#endif

    // CHECK IF PROGRAM'S FOLDER EXISTS / CREATE ONE
    QString qtpath = path.c_str();
    QDir dir(qtpath);
    if (!dir.exists()) {
      dir.mkpath(qtpath);
    }

    path += filename;
    return path;
}

std::vector<std::string> MainWindow::ReadFile(std::string filename) {
    std::string path = GetFile(filename);
    std::vector<std::string> data;

    std::string line;
    std::ifstream file(path);
    if (file.is_open()) {
        while ( getline (file,line) ) {
            data.push_back(line);
        }
    }
    return data;
}

void MainWindow::UpdateFile(std::string filename, std::string data) {
    std::string path = GetFile(filename);

    std::ofstream file(path);
    file << data;
    file.close();
}

void MainWindow::closeEvent(QCloseEvent*){
    tabs->OnWindowClose();
    QApplication::closeAllWindows();
}

void MainWindow::dragEnterEvent(QDragEnterEvent* e){
    e->acceptProposedAction();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent* e){
        e->accept();
    }

void MainWindow::dropEvent(QDropEvent* e)
{
    DragTabData* data = dynamic_cast<DragTabData*>(e->source());
    if(data && data->tbar == tabs->tabBar())
        return;

    e->setDropAction(Qt::MoveAction);
    e->accept();
    tabs->addTab(data->tab,data->tabname);
    data->tryCloseWindow();
}


void MainWindow::CheckDarkTheme() {
    #ifdef DISCONNECT_OS_WIN

        QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize",QSettings::NativeFormat);
        if(settings.value("AppsUseLightTheme")==0) {
            qApp->setStyle(QStyleFactory::create("Fusion"));
            QPalette darkPalette;
            QColor darkColor = QColor(45,45,45);
            QColor disabledColor = QColor(127,127,127);
            darkPalette.setColor(QPalette::Window, darkColor);
            darkPalette.setColor(QPalette::WindowText, Qt::white);
            darkPalette.setColor(QPalette::Base, QColor(30,30,30));
            darkPalette.setColor(QPalette::AlternateBase, darkColor);
            darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
            darkPalette.setColor(QPalette::ToolTipText, Qt::white);
            darkPalette.setColor(QPalette::Text, Qt::white);
            darkPalette.setColor(QPalette::Disabled, QPalette::Text, disabledColor);
            darkPalette.setColor(QPalette::Button, darkColor);
            darkPalette.setColor(QPalette::ButtonText, Qt::white);
            darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, disabledColor);
            darkPalette.setColor(QPalette::BrightText, Qt::red);
            darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));

            darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
            darkPalette.setColor(QPalette::HighlightedText, Qt::black);
            darkPalette.setColor(QPalette::Disabled, QPalette::HighlightedText, disabledColor);

            qApp->setPalette(darkPalette);

            qApp->setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");
        }

    #endif
}
