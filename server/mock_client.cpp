#include "..\disconnect_sock.hpp"
#include <cstdio>
#include <chrono>
#include <thread>

void sleepfor(size_t t)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(t));
}

const char* serverip = "127.0.0.1";
const uint16_t serverport = 8888;

int main()
{
    dSetup();

    d_socket sock = socket(AF_INET,SOCK_STREAM,0);

    sockaddr_in addrin;memset(&addrin,0,sizeof(addrin));
    addrin.sin_family = AF_INET;
    addrin.sin_addr.s_addr = inet_addr(serverip);
    addrin.sin_port = htons(serverport);

    if(connect(sock,(sockaddr*)&addrin,sizeof(addrin)) != 0)
    {
        printf("connect failed: %d\n",dLastError());
        return 0;
    }

    sleepfor(500);

    char msg_buffer[200] = {0};

    //handshake

    *((DMessageHeader*)msg_buffer) = {DMessageCode::Handshake1,sizeof(DMessageHeader)};

    if(send(sock,msg_buffer,sizeof(DMessageHeader),0) == SOCKET_ERROR)
    {
        printf("send handshake1 failed: %d\n",dLastError());
        return 0;
    }
    sleepfor(500);


    int bytes = recv(sock,msg_buffer,sizeof(msg_buffer),0);

    if(bytes == 0)
    {
        puts("connection ended gracefully");
        return 0;
    }
    else if(bytes == SOCKET_ERROR)
    {
        printf("recv failed: %d\n",dLastError());
        return 0;
    }

    printf("received: %u, %u\n",(uint32_t)((DMessageHeader*)msg_buffer)->code,((DMessageHeader*)msg_buffer)->size);

    //register

    const char* username = "teromies";
    const char* password = "salasana321";

    uint32_t siz = sizeof(DMessageHeader)+9+12+sizeof(DUserInfoHeader);

    *(DMessageHeader*)msg_buffer = DMessageHeader{DMessageCode::RegisterRequest,siz};

    *(DUserInfoHeader*)(msg_buffer+sizeof(DMessageHeader)) = DUserInfoHeader{9,12};

    memcpy(msg_buffer+sizeof(DMessageHeader)+sizeof(DUserInfoHeader),username,9);
    memcpy(msg_buffer+sizeof(DMessageHeader)+sizeof(DUserInfoHeader)+9,password,12);

    if(send(sock,msg_buffer,siz,0) == SOCKET_ERROR)
    {
        printf("send register failed: %d\n",dLastError());
        return 0;
    }

    bytes = recv(sock,msg_buffer,sizeof(msg_buffer),0);

    if(bytes == 0)
    {
        puts("connection ended gracefully");
        return 0;
    }
    else if(bytes == SOCKET_ERROR)
    {
        printf("recv failed: %d\n",dLastError());
        return 0;
    }

    printf("received: %u, %u\n",(uint32_t)((DMessageHeader*)msg_buffer)->code,((DMessageHeader*)msg_buffer)->size);

    if(((DMessageHeader*)msg_buffer)->code == DMessageCode::BadRegister)
    {
        printf("Got BadRegister, with error: %s\n",msg_buffer+sizeof(DMessageHeader));
        return 0;
    }

    //send chatmessage
    char test_message[] = "this is a test message sent by mock client!";
    
    uint32_t test_msg_size = sizeof(DMessageHeader)+sizeof(DChatMessageHeader1)+sizeof(test_message);

    *((DMessageHeader*)msg_buffer) = DMessageHeader{DMessageCode::ChatMessage,test_msg_size};
    *((DChatMessageHeader1*)(msg_buffer+sizeof(DMessageHeader))) = DChatMessageHeader1{0,sizeof(test_message)};


    memcpy(msg_buffer+sizeof(DMessageHeader)+sizeof(DChatMessageHeader1),test_message,sizeof(test_message));

    if(send(sock,msg_buffer,test_msg_size,0) == SOCKET_ERROR)
    {
        printf("send test_message failed: %d\n",dLastError());
        return 0;
    }
    if(send(sock,msg_buffer,test_msg_size,0) == SOCKET_ERROR)
    {
        printf("send test_message failed: %d\n",dLastError());
        return 0;
    }
    //sleepfor(100);

    closesocket(sock);

    puts("ended connection");

    dCleanup();
    return 0;
}