#include "../disconnect_sock.hpp"
#include <iostream>
#include <cstdint>
#include <thread>
#include <mutex>
#include <future>
#include <vector>
#include <unordered_map>
#include <queue>
#include <chrono>
#include <memory>
#include <algorithm>
#include <tuple>

//- should client connection have another thread for sending?
//- it could be a worker thread, and do like 1ms sleep every cycle, waiting to see if 
//  some queue has incoming messages
//- could use shared pointers for the messages, so all threads could use the same one,
//  and it would be automatically deleted
//- would need to mutex the socket though
//- and ofc if client can't receive messages rn, like if it's doing handshake or
//  login or anything but idle, the message sender should just throw the message away.
//  Well, maybe wait with the send, if client is in the middle of a keep alive or something.
//- Anyway client should request previous messages when it joins a channel, and this
//  should ofc happen after login too.

//TODO:
//  - add debug messages to login and register

//Error messages (long ones, which are maybe used more than once)
const char* error_missing_port = "Missing port number: Set the port number with --port option";
const char* error_invalid_port = "Invalid port number: Make sure port number only contains numbers";

const char* localip = "127.0.0.1";

const char* badlogin1_message = "Invalid username or password";
constexpr uint32_t badlogin1_len = 29u;

const char* badlogin2_message = "Could not login";
constexpr uint32_t badlogin2_len = 16u;

const char* badregister1_message = "Username is already taken";
constexpr uint32_t badregister1_len = 26u;

const char* badregister2_message = "Password must be at least 5 characters long";
constexpr uint32_t badregister2_len = 44u;

//wrapper for dynamic c arrays, with array size
template<class T>
struct array
{
    array(uint32_t len)
    : length(len), ptr(new T[len]) 
    {
        //std::cout << "Created array\n";
    }

    ~array(){
        //std::cout << "Deleted array\n";
        delete ptr;
    }

    uint32_t size() const noexcept{
        return length;
    }

    T& operator[] (uint32_t i) noexcept{
        return ptr[i];
    }

    const T& operator[] (uint32_t i) const noexcept{
        return ptr[i];
    }

    T& operator*() noexcept{
        return *ptr;
    }

    const T& operator*() const noexcept{
        return *ptr;
    }

    //for compatibility
    T* data() noexcept{
        return ptr;
    }

    const T* data() const noexcept{
        return ptr;
    }

    const uint32_t length;
    T* ptr;
};

struct ClientConnection
{
    //thread
    //socket
    //session token
    //etc.

    ~ClientConnection() = default;

    std::string username;

    std::thread client_thread;

    d_socket socket = INVALID_SOCKET;
    DServerConnectionState state = DServerConnectionState::Handshake1;
    bool can_recv_chatmessage = false;
    std::mutex can_recv_chatmessage_mutex;

    //Chatmessages should probably be shared_ptr for efficiency
    //message type undecided. probably should be a char* for easier handling
    std::vector<std::pair<std::shared_ptr<array<char>>,std::shared_ptr<ClientConnection>>> waiting_chatmessages;
    std::mutex waiting_chatmessages_mutex;


    //is one waiting message enough? 
    std::vector<std::shared_ptr<array<char>>> waiting_servermessages;
    bool has_waiting_servermessage = false;
    std::mutex waiting_servermessage_mutex; //one mutex for both message and bool

};

auto sleepfor(size_t t)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(t));
}

template<class _T, class _Predicate>
void erase_if(std::vector<_T>& v, _Predicate p)
{
    v.erase(std::remove_if(v.begin(),v.end(),p),v.end());
}

std::mutex cout_mutex;

//Fancy messages
template<class MsgT, class... T>
void ServerPrint(const char* prefix, MsgT msg, T... args)
{
    std::lock_guard<std::mutex> lock(cout_mutex);
    std::cout << '[' << prefix << ']' << ' ';
    std::cout << msg;
    ((std::cout << args),...);
    std::cout << std::endl; 
}

//Error message wrapper
template<class MsgT, class... T>
void ServerError(MsgT msg, T... args)
{
    const char* pref = "Error";
    ServerPrint(pref,msg,args...);
}

//Server message wrapper
template<class MsgT, class... T>
void ServerMessage(MsgT msg, T... args)
{
    const char* pref = "Server";
    ServerPrint(pref,msg,args...);
}

#define DISCONNECT_SERVER_DEBUG_LEVEL 3

template<class MsgT, class... T>
void ServerDebug(MsgT msg, T... args)
{
    #if defined(DISCONNECT_SERVER_DEBUG_LEVEL) && DISCONNECT_SERVER_DEBUG_LEVEL > 2
    const char* pref = "Debug";
    ServerPrint(pref,msg,args...);
    #endif
}

//Convert cstring to a port
//no negative numbers allowed, port number must be <= UINT16_MAX
d_port_t GetPort(const char* port_str)
{
    unsigned i = 0;
    while(port_str[i])
    {
        if(!isdigit(port_str[i]))
        {
            ServerError(error_invalid_port);
            return 0;
        }
        ++i;
    }

    unsigned long res = strtoul(port_str,0,10);

    if(!res || res > UINT16_MAX)
    {
        ServerError(error_invalid_port);
        return 0;
    }

    return (d_port_t)res;
}


//server variables
std::vector<std::pair<std::shared_ptr<array<char>>,std::shared_ptr<ClientConnection>>> incoming_messages;
std::vector<std::pair<std::shared_ptr<array<char>>,std::shared_ptr<ClientConnection>>> tmp_messages;
std::mutex incoming_messages_mutex;

std::vector<std::shared_ptr<ClientConnection>> client_connections;
std::mutex client_connections_mutex;

std::thread message_handler;
std::thread connection_listener;

std::promise<void> server_exit_signal;
std::shared_future<void> server_exit_future;

//temporary user db: <username,password>
//should use sql or something really
std::unordered_map<std::string,std::string> users;
std::mutex users_mutex;

//currently logged in users names <name,dummy>
std::unordered_map<std::string,char> active_users;
std::mutex active_users_mutex;

uint32_t GenerateMessageID()
{
    std::atomic<uint32_t> gen = 0;

    return ++gen;
}

//returns send-ready messages for message confirm and broadcastable chat message
//1st return parameter is success bool
std::tuple<bool,std::shared_ptr<array<char>>,std::shared_ptr<array<char>>> HandleChatMessage(const std::string& sender, const char* data, uint32_t data_len)
{
    //data format:
    //  DChatMessageHeader1
    //  message

    DChatMessageHeader1*  msg = (DChatMessageHeader1*)data;

    //bad data, message len is too long
    if(msg->msg_size > data_len-sizeof(DChatMessageHeader1))
    {
        return std::make_tuple(false,std::make_shared<array<char>>(0),std::make_shared<array<char>>(0));
    }

    uint32_t id = GenerateMessageID();

    constexpr uint32_t confirm_size = sizeof(DMessageHeader)+sizeof(DMessageConfirm);
    
    auto confirm = std::make_shared<array<char>>(confirm_size);

    uint16_t name_len = (uint16_t)(sender.length()+1);

    *(DMessageHeader*)(confirm->data()) = DMessageHeader{DMessageCode::ChatMessageConfirm,confirm_size};
    *(DMessageConfirm*)(confirm->data()+sizeof(DMessageHeader)) = DMessageConfirm{msg->t_id,id,msg->channel_id};

    uint16_t msg_size = sizeof(DMessageHeader)+sizeof(DChatMessageHeader2)+name_len+msg->msg_size;

    auto n_msg = std::make_shared<array<char>>(msg_size);

    //Set main header
    *(DMessageHeader*)(n_msg->data()) = DMessageHeader{DMessageCode::ChatMessage,msg_size};

    //Set message header
    *(DChatMessageHeader2*)(n_msg->data()+
    sizeof(DMessageHeader)) = DChatMessageHeader2{id,msg->channel_id,name_len,msg->msg_size};

    //Set username
    memcpy(n_msg->data()+
    sizeof(DMessageHeader)+
    sizeof(DChatMessageHeader2),sender.data(),name_len);

    //Set message
    memcpy(n_msg->data()+
    sizeof(DMessageHeader)+
    sizeof(DChatMessageHeader2)+
    name_len,data+sizeof(DChatMessageHeader1),msg->msg_size);

    return std::make_tuple(true,confirm,n_msg);
}

void QueueClientMessage(std::shared_ptr<ClientConnection> connection, std::shared_ptr<array<char>> msg)
{
    connection->has_waiting_servermessage = true;
    connection->waiting_servermessage_mutex.lock();
    connection->waiting_servermessages.push_back(msg);
    connection->waiting_servermessage_mutex.unlock();
}

void ClientConnectionReceive(std::shared_ptr<ClientConnection> connection, std::shared_future<void> exit_fut, char* msg_buffer, uint32_t buffer_size, uint32_t& bytes_read)
{
    int32_t bytes = (int32_t)recv(connection->socket,msg_buffer+bytes_read,buffer_size-bytes_read,0);
    bool prev = bytes_read && 1;

    if(bytes == SOCKET_ERROR)
    {
        auto err = dLastError();

        switch (err)
        {
            //wouldblock = no data received
            case D_EWOULDBLOCK:
            {
                if(bytes_read != 0)
                    break;
                else
                    return;
            }
            //handle other errors and disconnect here

            case D_ECONNRESET:
            {
                //connection ended
                connection->state = DServerConnectionState::Terminated;
                return;
            }
        }
        //whatever the error code, either continue or return from here
        //don't move on to read a whole message 
    }
    else if(bytes == 0 && bytes_read == 0)
    {
        ServerDebug("Connection end_0");
        //connection ended
        connection->state = DServerConnectionState::Terminated;
        return;
    }
    ServerDebug("Message received! Initial read: ", bytes, ", bytes_read: ", bytes_read);
    ServerDebug("Msg size from header: ", ((DMessageHeader*)msg_buffer)->size);
    
    bool has_header = false;
    bool msg_ok = false;
    uint32_t msg_size = 0;
    uint32_t bytes_remaining = -1;

    auto get_header = [&]()
    {
        has_header = true;
        msg_size = ((DMessageHeader*)msg_buffer)->size;
        bytes_remaining = bytes_read>=msg_size?0:(msg_size-bytes_read);
        ServerDebug("Bytes remaining: ", bytes_remaining);
    };
    
    if(bytes >= (int32_t)sizeof(DMessageHeader))    //ald least header was read
    {
        bytes_read += bytes;
        get_header();
    }
    //nothing was read now, but at least header was read previously
    else if((bytes == SOCKET_ERROR || bytes == 0) && prev && bytes_read >= sizeof(DMessageHeader))
    {
        get_header();
    }
    
    auto timeout_t1 = std::chrono::high_resolution_clock::now();

    if(bytes_remaining == 0)
        goto msg_read_done;
    
    while(exit_fut.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout)
    {
        //check for timeout (20 seconds)
        auto timeout_t2 = std::chrono::high_resolution_clock::now();
        auto t = std::chrono::duration_cast<std::chrono::milliseconds>(timeout_t2-timeout_t1);

        if(t.count() > server_timeout)
        {
            //timed out
            break;
        }

        bytes = (int32_t)recv(connection->socket,msg_buffer+bytes_read,buffer_size-bytes_read,0);

        if(bytes == SOCKET_ERROR)
        {
            if(dLastError() == D_EWOULDBLOCK)
                continue;

            ServerDebug("Error receiving rest of message: ", dLastError(), ", read: ", bytes_read);
            //pretty much any errors here would be fatal I think
            connection->state = DServerConnectionState::Terminated;
            return;
        }
        else if(bytes == 0)
        {
            ServerDebug("Connection end_1");
            //connection ended
            connection->state = DServerConnectionState::Terminated;
            return;
        }

        if(has_header)
        {
            bytes_read += bytes;
        }
        else
        {
            bytes_read += bytes;
            get_header();
        }

        if(bytes_read == msg_size)
        {
            msg_ok = true;
            break;
        }
    }

    if(!msg_ok)
    {
        return;
    }

    msg_read_done:

    //Switch on connection state.
    //Each case can expect 1 or more messages, so they can
    //have switches also.

    ServerDebug("Message ok");
    DMessageHeader* header = (DMessageHeader*)msg_buffer;

    //If received message:
    switch (connection->state)
    {
        case DServerConnectionState::Handshake1:
        {
            ServerDebug("Received Handshake1");
            //expect client handshake initiation
            
            //TESTPHASE: don't do any complicated handshake
            //  - when server gets this message, it moves to Idle state
            if(header->code == DMessageCode::Handshake1)
            {
                //connection->state = DServerConnectionState::Idle;
                connection->state = DServerConnectionState::PreLogin;


                //resize to message size. In this case just the header
                //connection->waiting_servermessages.resize(sizeof(DMessageHeader));

                //zero out the message
                //std::fill(connection->waiting_servermessage.begin(),connection->waiting_servermessage.end(),0);

                auto msg = std::make_shared<array<char>>(sizeof(DMessageHeader));
                //memset(msg->data(),0,sizeof(DMessageHeader));

                //Write message data
                *(DMessageHeader*)(msg->data()) = DMessageHeader{DMessageCode::Handshake1,sizeof(DMessageHeader)};
                QueueClientMessage(connection,msg);

                ServerDebug("Response ready");
            }
            else
            {
                //expected handshake, got something else -> terminate
                ServerDebug("Connection end_2");
                connection->state = DServerConnectionState::Terminated;
            }
            break;
        }
        //do rest of handshake

        //handshake done, not logged in
        case DServerConnectionState::PreLogin:
        {
            //expect login request or register request

            if(header->code == DMessageCode::LoginRequest || header->code == DMessageCode::RegisterRequest)
            {
                DUserInfoHeader* userinfo = (DUserInfoHeader*)(msg_buffer+sizeof(DMessageHeader));
                char* data = msg_buffer+sizeof(DMessageHeader)+sizeof(DUserInfoHeader);

                auto username = std::string(data);
                auto passw = std::string(data+userinfo->name_len);

                if(header->code == DMessageCode::LoginRequest)
                {
                    //check username + password combo from database
                    //login if match

                    const char* err;
                    uint32_t err_len = 0;

                    users_mutex.lock();
                    if(users.find(username) != users.end())
                    {
                        active_users_mutex.lock();
                        if(active_users.find(username) == active_users.end())
                        {
                            if(users[username] == passw)
                            {
                                active_users[username] = 0;
                                active_users_mutex.unlock();
                                users_mutex.unlock();
                                //successful login
                                connection->username = username;

                                auto msg = std::make_shared<array<char>>(sizeof(DMessageHeader));

                                *(DMessageHeader*)(msg->data()) = DMessageHeader{DMessageCode::GoodLogin,sizeof(DMessageHeader)};
                                QueueClientMessage(connection,msg);
                                connection->state = DServerConnectionState::Idle;
                                ServerDebug("Logged in, going to idle");
                                break;
                            }
                            else
                            {
                                active_users_mutex.unlock();
                                users_mutex.unlock();
                                err = badlogin1_message;
                                err_len = badlogin1_len;
                            }
                        }
                        else
                        {
                            active_users_mutex.unlock();
                            users_mutex.unlock();
                            err = badlogin2_message;
                            err_len = badlogin2_len;
                        }
                    }
                    else
                    {
                        users_mutex.unlock();
                        err = badlogin1_message;
                        err_len = badlogin1_len;
                    }
                    
                    uint16_t msg_len = err_len+sizeof(DMessageHeader);
                    auto msg = std::make_shared<array<char>>(msg_len);

                    *(DMessageHeader*)(msg->data()) = DMessageHeader{DMessageCode::BadLogin,msg_len};
                    memcpy(msg->data()+sizeof(DMessageHeader),err,err_len);
                    QueueClientMessage(connection,msg);

                }
                else if(header->code == DMessageCode::RegisterRequest)
                {
                    //check that username is not in use, and add user if so
                    //if username is in use, send back BadRegister with "Username is taken"

                    const char* err;
                    uint32_t err_len = 0;
                    
                    users_mutex.lock();
                    if(users.find(username) == users.end())
                    {
                        if(passw.length() >= 5)
                        {
                            //successful register

                            users[username] = passw;
                            users_mutex.unlock();

                            connection->username = username;

                            auto msg = std::make_shared<array<char>>(sizeof(DMessageHeader));

                            *(DMessageHeader*)(msg->data()) = DMessageHeader{DMessageCode::GoodRegister,sizeof(DMessageHeader)};
                            QueueClientMessage(connection,msg);
                            connection->state = DServerConnectionState::Idle;
                            ServerDebug("Registered, going to idle");
                            break;
                        }
                        else 
                        {
                            users_mutex.unlock();
                            err = badregister2_message;
                            err_len = badregister2_len;
                        }
                    }
                    else
                    {
                        users_mutex.unlock();
                        err = badregister1_message;
                        err_len = badregister1_len;
                    }
                    
                    uint16_t msg_len = err_len+sizeof(DMessageHeader);

                    auto msg = std::make_shared<array<char>>(msg_len);

                    *(DMessageHeader*)(msg->data()) = DMessageHeader{DMessageCode::BadRegister,msg_len};
                    memcpy(msg->data()+sizeof(DMessageHeader),err,err_len);
                    QueueClientMessage(connection,msg);
                }
            }
            else
            {
                //terminate if got some other message
                ServerDebug("Connection end_3");
                connection->state = DServerConnectionState::Terminated;
            }
            break;
        }

        //connection established, logged in
        case DServerConnectionState::Idle:
        {
            switch (header->code)
            {
                case DMessageCode::ChatMessage:
                {
                    ServerDebug("Chat message! size: ", msg_size);
                    //should print message?
                    char* message = msg_buffer+sizeof(DMessageHeader);
                    //ServerMessage(message);

                    auto [success,confirm,n_msg] = HandleChatMessage(connection->username,message, msg_size-sizeof(DMessageHeader));
                    
                    //ServerDebug("Handled message");

                    if(success)
                    {
                        QueueClientMessage(connection,confirm);

                        //ServerDebug("Queued return message");

                        incoming_messages_mutex.lock();
                        incoming_messages.push_back(std::make_pair(n_msg,connection));
                        incoming_messages_mutex.unlock();
                    }
                    else    //error with message handling
                    {
                        auto errmsg = std::make_shared<array<char>>(sizeof(DMessageHeader));

                        *(DMessageHeader*)(errmsg->data()) = DMessageHeader{DMessageCode::ChatMessageError,sizeof(DMessageHeader)};

                        QueueClientMessage(connection,errmsg);
                    }
                    //incoming_messages_mutex.lock();
                    //auto arr = std::make_shared<array<char>>(msg_size);
                    //memcpy(arr->data(),message,msg_size);
                    //incoming_messages.push_back(std::make_pair(arr,connection));
                    //incoming_messages_mutex.unlock();
                    break;
                }
            }
            break;
        }
    }

    //if more was read than the message, pass it on to the next cycle
    if(bytes_read > msg_size)
    {
        memmove(msg_buffer,msg_buffer+msg_size,recv_buffer_size-msg_size);
        bytes_read -= msg_size;
    }
    else
    {
        bytes_read = 0;
    }
}

void ClientConnectionSend(std::shared_ptr<ClientConnection> connection, std::shared_future<void> exit_fut)
{
    //send servermessage first

    static u_long flag1 = 1, flag0 = 0;
    dIoctlsocket(connection->socket,FIONBIO,&flag0);

    if(connection->has_waiting_servermessage)
    {
        uint32_t index = 0;

        while(exit_fut.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout)
        {
            if(index == connection->waiting_servermessages.size())
            {
                break;
            }

            auto msg = connection->waiting_servermessages[index];
            ++index;

            ServerDebug("Sending server message");
            auto timeout_t1 = std::chrono::high_resolution_clock::now();
            while(exit_fut.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout)
            {
                if(send(connection->socket,msg->data(),msg->size(),0) != SOCKET_ERROR)
                {
                    break;
                }
                else //handle errors
                {
                    auto err = dLastError();
                    if(err == D_ECONNRESET || err == D_ETIMEDOUT)
                    {
                        //connection ended
                        connection->state = DServerConnectionState::Terminated;
                        goto ret;
                    }
                    else
                    {
                        connection->state = DServerConnectionState::Terminated;
                        goto ret;
                    }
                }
                
                auto timeout_t2 = std::chrono::high_resolution_clock::now();
                auto t = std::chrono::duration_cast<std::chrono::milliseconds>(timeout_t2-timeout_t1);

                if(t.count() > server_timeout)  //timed out -> end connection
                {
                    connection->state = DServerConnectionState::Terminated;
                    goto ret;
                }
            }
        }
        connection->waiting_servermessages.clear();

        ServerDebug("Sent message");
        connection->has_waiting_servermessage = false;
    }

    //send chatmessages second
    //only send chatmessages in idle state
    if(connection->state == DServerConnectionState::Idle)
    {
        uint32_t index = 0;
        std::vector<std::pair<std::shared_ptr<array<char>>,std::shared_ptr<ClientConnection>>> tmp_msg;

        if(connection->waiting_chatmessages.empty())
            goto ret;
        
        connection->waiting_chatmessages_mutex.lock();
        tmp_msg = std::move(connection->waiting_chatmessages);
        connection->waiting_chatmessages_mutex.unlock();

        while(exit_fut.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout)
        {
            if(index == tmp_msg.size())
            {
                break;
            }

            //this will go out of scope, and since it's a shared_ptr we can ignore it
            //auto msg = connection->waiting_chatmessages.front(); 
            //connection->waiting_chatmessages.pop();
            
            auto msg = tmp_msg[index];
            ++index;

            if(msg.second == connection)
                continue;

            auto timeout_t1 = std::chrono::high_resolution_clock::now();
            while(exit_fut.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout)
            {
                
                if(send(connection->socket,msg.first->data(),msg.first->size(),0) != SOCKET_ERROR)
                {
                    break;
                }
                else //handle errors
                {
                    auto err = dLastError();
                    if(err == D_ECONNRESET || err == D_ETIMEDOUT)
                    {
                        //connection ended
                        connection->state = DServerConnectionState::Terminated;
                        goto ret;
                    }
                }
                
                auto timeout_t2 = std::chrono::high_resolution_clock::now();
                auto t = std::chrono::duration_cast<std::chrono::milliseconds>(timeout_t2-timeout_t1);

                if(t.count() > server_timeout)  //timed out -> end connection
                {
                    connection->state = DServerConnectionState::Terminated;
                    goto ret;
                }
            }
        }
    }
    ret:
    dIoctlsocket(connection->socket,FIONBIO,&flag1);
}

void ClientConnectionLoop(std::shared_ptr<ClientConnection> connection, std::shared_future<void> exit_fut)
{
    //recv buffer
    char* msg_buffer = (char*)malloc(sizeof(char)*recv_buffer_size);

    uint32_t bytes_read = 0;

    //set to non blocking
    static u_long flag = 1;
    dIoctlsocket(connection->socket,FIONBIO,&flag);

    ServerDebug("Client connection started");

    while(exit_fut.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout && connection->state != DServerConnectionState::Terminated)
    {
        ClientConnectionReceive(connection,exit_fut,msg_buffer,recv_buffer_size,bytes_read);

        if(connection->state == DServerConnectionState::Terminated)
            break;

        //skip send if there is another message partly received
        if(bytes_read > 0)
            continue;
            
        ClientConnectionSend(connection, exit_fut);
    }
    closesocket(connection->socket);

    active_users_mutex.lock();
    active_users.erase(connection->username);
    active_users_mutex.unlock();

    free(msg_buffer);

    ServerDebug("Connection ended");
}

//listen for and accept incoming connections
void ListenForConnections(d_port_t port, std::shared_future<void> exit_fut)
{
    d_socket listener_socket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

    sockaddr_in addrin;memset(&addrin,0,sizeof(addrin));
    addrin.sin_family = AF_INET;
    addrin.sin_addr.s_addr = INADDR_ANY; 
    addrin.sin_port = htons(port);

    if(bind(listener_socket,(sockaddr*)&addrin,sizeof(addrin)))
    {
        ServerError("Failed to bind listener socket: ",dLastError());
        return;
    }

    //set to non blocking. most likely listen will not block, but it technically can.
    u_long flag = 1;
    dIoctlsocket(listener_socket,FIONBIO,&flag);
    
    listen(listener_socket,5);
    ServerDebug("Begun listening");

    d_socket conn_sock = INVALID_SOCKET;

    while(exit_fut.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout)
    {
        if((conn_sock = accept(listener_socket,NULL,NULL)) == INVALID_SOCKET)
        {
            auto err = dLastError();

            if(err == D_EWOULDBLOCK)
                continue;
            else{
                ServerError("Unexpected error in Listen: ", err);
                sleepfor(1000);
            }
        }
        else
        {
            std::shared_ptr<ClientConnection> new_client = std::make_shared<ClientConnection>();
            new_client->socket = conn_sock;

            new_client->client_thread = 
            std::thread(ClientConnectionLoop,new_client,exit_fut);

            client_connections_mutex.lock();
            client_connections.push_back(new_client);
            client_connections_mutex.unlock();

            new_client = nullptr;
            conn_sock = INVALID_SOCKET;
            ServerDebug("New client");
        }
    }

    closesocket(listener_socket);
}

void ChatMessageBroadcaster(std::shared_future<void> exit_fut)
{
    while(exit_fut.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout)
    {
        std::vector<std::pair<std::shared_ptr<array<char>>,std::shared_ptr<ClientConnection>>> tmp_msg;

        incoming_messages_mutex.lock();
        tmp_msg = std::move(incoming_messages);
        incoming_messages_mutex.unlock();

        if(!tmp_msg.empty())
        {
            /*std::vector<std::pair<std::shared_ptr<array<char>>,std::shared_ptr<ClientConnection>>> tmp_msg;

            for(auto& m : tmp_msg1)
            {
                uint32_t msg_size = m.first->size()+sizeof(DMessageHeader);
                //add message metadata here, like sender name, etc
                auto a = std::make_pair(std::make_shared<array<char>>(msg_size),m.second);
                *((DMessageHeader*)a.first->data()) = DMessageHeader{DMessageCode::ChatMessage,msg_size};
                memcpy(a.first->data()+sizeof(DMessageHeader),m.first->data(),m.first->size());
                tmp_msg.push_back(a);
            }
            */

            client_connections_mutex.lock();
            for(auto& con : client_connections)
            {
                if(con->state == DServerConnectionState::Terminated)
                    continue;

                con->waiting_chatmessages_mutex.lock();
                con->waiting_chatmessages.insert(con->waiting_chatmessages.end(),tmp_msg.begin(),tmp_msg.end());
                con->waiting_chatmessages_mutex.unlock();

            }
            client_connections_mutex.unlock();
        }

        //should probably not do this every time
        client_connections_mutex.lock();
        auto rm = std::remove_if(client_connections.begin(),client_connections.end(),
            [](std::shared_ptr<ClientConnection> c)
            { 
                bool res = c->state == DServerConnectionState::Terminated;
                if(res)
                {
                    if(c->client_thread.joinable())
                        c->client_thread.join();
                }
                return res;
            }
        );
        
        //for(auto it = rm; it != client_connections.end(); ++it)
        //{
        //    ServerDebug("Closing thread...");
        //    if((*it)->client_thread.joinable())
        //        (*it)->client_thread.join();
        //    ServerDebug("Closed thread");
        //}

        client_connections.erase(rm,client_connections.end());

        client_connections_mutex.unlock();
    }
}

int main(int argc, char** argv)
{
    d_port_t port = 0;

    //parse cmdline args (just port for now)
    if(argc < 2)
    {
        ServerError(error_missing_port);
        return 1;
    }
    else
    {
        for(int i = 1; i < argc; ++i)
        {
            if(!strcmp("--port",argv[i]))
            {
                if(i+1 < argc)
                {
                    port = GetPort(argv[i+1]);

                    if(!port){
                        return 1;
                    }
                    ++i;
                }
                else
                {
                    ServerError(error_missing_port);
                    return 1;
                }
            }
            else
            {
                ServerMessage("Unknown argument: ", argv[i]);
            }
        }
    }

    //do setup
    auto err = dSetup();
    if(err != ERROR_OK){
        ServerError("Setup Failed: ", err);
        return 1;
    }

    //get the future for the exit signal
    //exit signal will cause all threads to gracefully exit
    server_exit_future = server_exit_signal.get_future();

    //start incoming message handler thread
    ServerDebug("Start broadcaster");
    message_handler = std::thread(ChatMessageBroadcaster,server_exit_future);

    ServerDebug("Start listener");
    //start the connection listener thread
    connection_listener = std::thread(ListenForConnections,port,server_exit_future);

    //test
    //sleepfor(2000);

    char buff[100];

    while(true)
    {
        std::cin.getline(buff,100);

        if(!strcmp(buff,"exit"))
            break;
    }

    server_exit_signal.set_value();

    if(connection_listener.joinable())
        connection_listener.join();
    
    if(message_handler.joinable())
        message_handler.join();

    //join client threads
    for(auto& cl : client_connections)
    {
        if(cl->client_thread.joinable())
            cl->client_thread.join();
    }
    
    ServerDebug("Exit application");
    dCleanup();

    return 0;
}