#ifndef LOGINSCREEN_H
#define LOGINSCREEN_H

#include <QDialog>

namespace Ui {
class LoginScreen;
}

class LoginScreen : public QDialog
{
    Q_OBJECT

public:
    explicit LoginScreen(int index, QString serverName, QWidget *parent = nullptr);
    ~LoginScreen();

private:
    Ui::LoginScreen *ui;
    int index;

private slots:
    void SendLoginData();
    void SendRegisterData();

signals:
    void ReturnData(int index, QString username, QString password, bool newUser);
};

#endif // LOGINSCREEN_H
