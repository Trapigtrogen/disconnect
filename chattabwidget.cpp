#include "chattabwidget.h"
#include "mainwindow.h"

void ChatTabWidget::mouseMoveEvent(QMouseEvent* e)
{
    if(e->buttons() != Qt::RightButton)
        return;

    QPoint gpos = mapToGlobal(e->pos());
    QTabBar* tbar = tabBar();
    QPoint posInT = mapFromGlobal(gpos);
    int index = tbar->tabAt(e->pos());
    QRect rct = tbar->tabRect(index);

    QPixmap* pmap = new QPixmap(rct.size());

    QPainter pain(pmap);

    tbar->render(&pain,QPoint(),QRegion(rct));

    pain.end();


    DragTabData* data = new DragTabData;
    data->tbar = tabBar();
    data->tab = widget(index);
    data->tabname = tabText(index);

    data->fromMain = 1 && dynamic_cast<MainWindow*>(window());

    TabDrag drag(data);

    drag.setMimeData(new QMimeData);
    drag.setPixmap(*pmap);

    delete pmap;

    drag.setHotSpot(e->pos() - posInT);
    drag.setDragCursor(QCursor(Qt::OpenHandCursor).pixmap(),Qt::MoveAction);
    auto result = drag.exec(Qt::MoveAction);
}
